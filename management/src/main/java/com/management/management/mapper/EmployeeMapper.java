package com.management.management.mapper;

import com.management.management.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 13:03
 */
@Mapper
public interface EmployeeMapper {
    //查询所有
    public List<Employee> selectEmployee();

    //按id查询
    public Employee selectOneEmployee(int id);

    //添加
    public int addEmployee(Employee employee);

    //删除
    public  int delectEmployee(int id);

    //修改
    public  int updateEmployee(int id);
}
