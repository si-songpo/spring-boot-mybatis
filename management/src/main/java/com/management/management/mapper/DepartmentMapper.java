package com.management.management.mapper;

import com.management.management.entity.Department;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 12:28
 */
@Mapper
public interface DepartmentMapper {
    public List<Department> selectDepartment();
}
