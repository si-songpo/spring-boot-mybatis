package com.management.management.entity;

import java.util.Date;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 12:58
 */
public class Employee {
    private int employee_id;
    private String employee_name;
    private String employee_email;
    private int employee_department;
    private Date employee_date;

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_email() {
        return employee_email;
    }

    public void setEmployee_email(String employee_email) {
        this.employee_email = employee_email;
    }

    public int getEmployee_department() {
        return employee_department;
    }

    public void setEmployee_department(int employee_department) {
        this.employee_department = employee_department;
    }

    public Date getEmployee_date() {
        return employee_date;
    }

    public void setEmployee_date(Date employee_date) {
        this.employee_date = employee_date;
    }

    public Employee(int employee_id, String employee_name, String employee_email, int employee_department) {
        this.employee_id = employee_id;
        this.employee_name = employee_name;
        this.employee_email = employee_email;
        this.employee_department = employee_department;
        this.employee_date=new Date();
    }
}
