package com.management.management.controller;

import com.management.management.entity.Department;
import com.management.management.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 12:31
 */
@RestController
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;

    @RequestMapping("/selectDepartment")
    public List<Department> selectDepartment(){
        return departmentService.selectDepartment();
    }
}
