package com.management.management.controller;

import com.management.management.entity.Employee;
import com.management.management.mapper.EmployeeMapper;
import com.management.management.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Scanner;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 13:23
 */
@RestController
public class EmployeeController {
    private static int Employee_ID = 15;
    Scanner scanner = new Scanner(System.in);
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/selectEmployee")
    public List<Employee> selectEmployee(){
        return employeeService.selectEmployee();
    }
    //按id查找
    @RequestMapping("/selectOneEmployee")
    public Employee selectOneEmployee(){
        System.out.println("请输入Id：");
        int id = scanner.nextInt();
        Employee_ID--;
        return employeeService.selectOneEmployee(id);
    }
    //添加
    @RequestMapping("/addEmployee")
    public String addEmployee(){
        Employee_ID++;
        employeeService.addEmployee(new Employee(Employee_ID,"王麻子","1241414.@qq.com",2));
        return "添加成功"+employeeService.selectOneEmployee(Employee_ID).toString();
    }
    //删除
    @RequestMapping("/delectEmployee")
    public String delectEmployee(){
        System.out.println("请输入Id：");
        int id = scanner.nextInt();
        employeeService.delectEmployee(id);
        return "删除成功";
    }

    //修改
    @RequestMapping("/updateEmployee")
    public String updateEmployee(){
        System.out.println("请输入Id：");
        int id = scanner.nextInt();
        employeeService.updateEmployee(id);
        return "修改成功";
    }
}
