package com.management.management.service;

import com.management.management.entity.Department;
import com.management.management.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 12:30
 */
@Service
public class DepartmentService {
    @Lazy
    @Autowired
    private DepartmentMapper departmentMapper;

    public List<Department> selectDepartment(){
        return departmentMapper.selectDepartment();
    }
}
