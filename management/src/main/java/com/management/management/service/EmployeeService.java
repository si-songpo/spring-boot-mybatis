package com.management.management.service;

import com.management.management.entity.Employee;
import com.management.management.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 唯一
 * @version 1.0
 * @date 2022/5/10 13:20
 */
@Service
public class EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;

    //查找全部
    public List<Employee> selectEmployee(){
        return employeeMapper.selectEmployee();
    }

    //按Id查找
    public Employee selectOneEmployee(int id){
        return employeeMapper.selectOneEmployee(id);
    }

    //添加
    public int addEmployee(Employee employee){
        return employeeMapper.addEmployee(employee);
    }

    //删除
    public int delectEmployee(int id){
        return employeeMapper.delectEmployee(id);
    }

    //修改
    public int updateEmployee(int id){
        return employeeMapper.updateEmployee(id);
    }

}
