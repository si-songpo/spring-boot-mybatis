/*
 Navicat Premium Data Transfer

 Source Server         : MySQl5
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : management

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 10/05/2022 18:00:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, '财务部');
INSERT INTO `department` VALUES (2, '销售部');
INSERT INTO `department` VALUES (3, '运营部');
INSERT INTO `department` VALUES (4, '生产部');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `employee_id` int(255) NOT NULL,
  `employee_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `employee_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `employee_department` int(255) NULL DEFAULT NULL,
  `employee_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`employee_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (1, 'wangmazi', '12345@qq.com', 2, '2022-05-04');
INSERT INTO `employee` VALUES (2, 'wangmazi', '12345@qq.com', 2, '2022-05-06');
INSERT INTO `employee` VALUES (3, '王五', 'saugdag@qq.com', 3, '2022-05-12');
INSERT INTO `employee` VALUES (4, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (5, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (6, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (7, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (8, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (9, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (10, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (13, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (14, '王麻子', '1241414.@qq.com', 2, '2022-05-10');
INSERT INTO `employee` VALUES (15, '王麻子', '1241414.@qq.com', 2, '2022-05-10');

SET FOREIGN_KEY_CHECKS = 1;
